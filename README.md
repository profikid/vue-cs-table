# cs-table

file is called ```CSTable.vue``` inside the Components folder
example implementation in App.vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Tests
```
npm run test:unit
```