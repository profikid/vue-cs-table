/* eslint-disable no-console */
import { shallowMount } from '@vue/test-utils';

import CSTable from '@/components/CSTable.vue';

describe('CSTable.vue', () => {

  let wrapper
  let checkboxes
  let selectAllCheckbox

  let mockData = [
    {name: 'smss.exe', device: 'Stark', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe', status: 'scheduled'}, 
    {nme: 'netsh.exe', device: 'Targaryen', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe', status: 'available'},
    {name: 'uxtheme.dll', device: 'Lanniester', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll', status: 'available'},
    {name: 'cryptbase.dll', device: 'Martell', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll', status: 'scheduled'},
    {name: '7za.exe', device: 'Baratheon', path: '\\Device\\HarddiskVolume1\\temp\\7za.exe', status: 'scheduled'}
  ]

  global.console = {
    error: jest.fn()
  }

  beforeEach(()=> {
    wrapper = shallowMount(CSTable,
      {
        propsData: {
          data: mockData
        }
      })
    checkboxes = wrapper.findAll('td input')
    selectAllCheckbox = wrapper.find('.select-all-checkbox')
  })
  it('should mount', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
    // expect(wrapper.isVueInstance).toBeFalsy()
  })
  it('should have no vue errors', () => {
    expect(console.error.mock.calls.length).toBe(0)
  });
  // it('Only those that have a status of "available" are currently able to be downloaded. Your implementation should manage this.', () => {
  //   expect(true).toBe(true)
  // })

  it('The select-all checkbox should be in an unselected state if no items are selected.', () => {
    let vm = wrapper.vm
    expect(checkboxes.length).toBe(vm.data.length)
    expect(checkboxes.at(0).element.checked).toBe(false)
    expect(checkboxes.at(1).element.checked).toBe(false)
    expect(checkboxes.at(2).element.checked).toBe(false)
    expect(checkboxes.at(3).element.checked).toBe(false)
    expect(checkboxes.at(4).element.checked).toBe(false)

    expect(selectAllCheckbox.element.checked).toBe(false)
    expect(checkboxes.length).toBe(vm.data.length)
  })

  it('The select-all checkbox should be in a selected state if all items are selected.', () => {
    expect(selectAllCheckbox.element.checked).toBe(false)

    selectAllCheckbox.element.click()

    expect(checkboxes.at(0).element.checked).toBe(true)
    expect(checkboxes.at(1).element.checked).toBe(true)
    expect(checkboxes.at(2).element.checked).toBe(true)
    expect(checkboxes.at(3).element.checked).toBe(true)
    expect(checkboxes.at(4).element.checked).toBe(true)
    expect(selectAllCheckbox.element.checked).toBe(true)
  })

  it('The select-all checkbox should be in an indeterminate state if some but not all items are selected.', () => {
    expect(selectAllCheckbox.element.checked).toBe(false)
    expect(selectAllCheckbox.element.indeterminate).toBe(false)
    checkboxes.at(0).element.click()
    checkboxes.at(1).element.click()
    expect(selectAllCheckbox.element.indeterminate).toBe(true)

  })

  it('The "Selected 2" text should reflect the count of selected items and display "None Selected" when there are none selected.', () => {
    wrapper.find('ul.cs-table-nav')
  })

  it('Clicking the select-all checkbox should select all items if none or some are selected.', () => {
    expect(selectAllCheckbox.element.checked).toBe(false)
    checkboxes.at(0).element.click()

    selectAllCheckbox.element.click()

    expect(checkboxes.at(0).element.checked).toBe(true)
    expect(checkboxes.at(1).element.checked).toBe(true)
    expect(checkboxes.at(2).element.checked).toBe(true)
    expect(checkboxes.at(3).element.checked).toBe(true)
    expect(checkboxes.at(4).element.checked).toBe(true)
    expect(selectAllCheckbox.element.checked).toBe(true)
  })

  it('Clicking the select-all checkbox should de-select all items if all are currently selected.', () => {
    selectAllCheckbox.element.click()
    expect(checkboxes.at(0).element.checked).toBe(true)
    expect(checkboxes.at(1).element.checked).toBe(true)
    expect(checkboxes.at(2).element.checked).toBe(true)
    expect(checkboxes.at(3).element.checked).toBe(true)
    expect(checkboxes.at(4).element.checked).toBe(true)
    selectAllCheckbox.element.click()
    expect(checkboxes.at(0).element.checked).toBe(false)
    expect(checkboxes.at(1).element.checked).toBe(false)
    expect(checkboxes.at(2).element.checked).toBe(false)
    expect(checkboxes.at(3).element.checked).toBe(false)
    expect(checkboxes.at(4).element.checked).toBe(false)

  })

  xit('Status should be correctly formatted', () => { })

  xit('Clicking "Download Selected" when some or all items are displayed should generate an alert box with the path and device of all selected files.', () => { })

  xit('Precise/exact HTML formatting/styling to match the mockup is not required however rows should change colour when selected and on hover.', () => { }) 
})
